## Accept a year input from the user and determine if it is a leap year or not.


def is_leap_year(year):
    if not isinstance(year, int):
        return "Input must be an integer."
    elif year <= 0:
        return "Input must be a positive integer greater than 0."
    elif year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
        return True
    else:
        return False

try:
    year = int(input("Enter a year: "))
    result = is_leap_year(year)
    if result:
        print(year, "is a leap year.")
    else:
        print(result)
except ValueError:
    print("Invalid input, Please enter a valid number")

#Accept two numbers, row and column from the user and create a grid of asterisks using the two numbers(row and col)

def create_grid(row, col):
    if not isinstance(row, int) or not isinstance(col, int):
        return "Both Inputs must be integers."
    elif row <= 0 or col <= 0:
        return "Both Inputs must be positive integers greater than 0."
    else:
        grid = ""
        for i in range(row):
            for j in range(col):
                grid += "*"
            grid += "\n"
        return grid

row = int(input("insert row\n"))
col = int(input("insert column\n"))
print(create_grid(row, col))