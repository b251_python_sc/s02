# [Section] taking input from user
# username = input("Please enter your name:\n")
# print(f"hello {username}! welcome to python short course")

# num1 = input("Enter first number : \n")
# num2 = input("Enter second number : \n")
# sum = int(num1) + int(num2) # makie it integer 
# print(f'sum of num1 and num2 is {sum}')
# print(f"the sum of num1 and num2 ins {int(num1)+int(num2)}")

# # if else statements 
# test_num = 75  ## without input 
# #test_num_input = input("input number for test:\n") #with input

# if int(test_num_input) >= 60:
#     print("Test Passed")
# else :
#     print("Test Failed")


# ### if else statements don't hav ecurly brackets but isntead rely on the spacing of the indention to denote the scope of the statement. It also uses the colon ":" to let the compiler know that the statement continues.
# test_num_2 = int(input("please enter the second test number:\n"))
# if test_num_2 > 0 : 
#     print("The number is positive")
# elif test_num_2 == 0:
#     print("The number is zero")
# else:
#     print("The number is negative")

# [Section] Loops 
# While loop 
i = 1 
while i <= 5:
    print (f"current count {i}") # expression to be executed 
    i += 1 #incrementor

# For loop 
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)

#range() function
# for x in range(6):
#     print(f"the current value is {x}")

# for x in range(5, 11):
#     print(f"the current value is {x}")

for x in range(5, 20, 2) :  #(start, end, interval)
    print(f"the current value is {x}")
# the current value is 9
# the current value is 11
# the current value is 13
# the current value is 15
# the current value is 17
# the current value is 19

# [Section] Break and Continue Statement
# j = 1 
# while j < 6 : 
#     print(j)
#     if j == 3 : 
#         break
#     j += 1 

k = 1 
while k < 6 :
    k += 1 
    if k == 3:
        continue
    print(k)
# 2
# 4
# 5
# 6
